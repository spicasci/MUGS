# MUGS - Model of Urban Green Spaces. 

An agent-based model of the emergence of inter-city and intra-city inequalities in the usage of green spaces.
The model is spatially explicit and data driven. It simulates Scottish cities in their 16+ population, urban form and distribution of green spaces and socio economic condition. The model is meant to explore to what extent a city's urban form and social mix may determine the emergence of a shared, cross-class culture of using or not using green spaces.

Last version is 0.5.9

## How to use

Download the content of the repository, open spans-0.5.9.nlogo in the latest version of NetLogo http://ccl.northwestern.edu/netlogo/6.2.0/ 
